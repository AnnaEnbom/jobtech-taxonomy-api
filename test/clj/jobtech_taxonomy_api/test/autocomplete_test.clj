(ns ^:integration-autocomplete-tests jobtech-taxonomy-api.test.autocomplete-test
  (:require [clojure.test :as test]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.test.db.database :as db]
            [jobtech-taxonomy-api.test.test-utils :as util]))

(test/use-fixtures :each util/fixture)

(def ^:private userid "Stallman")
(def ^:private endpoint "/v1/taxonomy/suggesters/autocomplete")

(defn createConcept [type definition preferred_label]
  (concepts/assert-concept userid {:type type
                                   :definition definition
                                   :preferred-label preferred_label}))

(defn createConcepts []
  (let [_ (doall (map (fn [[t d p]] (createConcept t d p))
                      [["skill" "" "testledare"]
                       ["skill" "" "krocktest"]
                       ["skill" "" "test"]
                       ["skill" "" "testpilot"]
                       ["skill" "" "läkemedelstestare"]
                       ["skill" "" "projektledare"]
                       ["skill" "" "allvetare"]
                       ["keyword" "" "testning"]
                       ["keyword" "" "sjunga"]]))]
    nil))

(test/deftest ^:integration-graphql-test-1 graphql-test-1
  (test/testing "test simple graphql queries"
    (let [_ (db/create-version-0)
          _ (createConcepts)
          _ (versions/create-new-version 1)
          [s1 b1] (util/send-request-to-json-service
                   :get endpoint
                   :query-params [{:key "query-string", :val "test"}])
          [s2 b2] (util/send-request-to-json-service
                   :get endpoint
                   :query-params [{:key "query-string", :val "test"}
                                  {:key "type", :val "skill"}])
          [s3 b3] (util/send-request-to-json-service
                   :get endpoint
                   :query-params [{:key "query-string", :val "test"}
                                  {:key "type", :val "keyword"}])
          [s4 b4] (util/send-request-to-json-service
                   :get endpoint
                   :query-params [{:key "query-string", :val "nomatch"}])]
      (test/is (= 200 s1))
      (test/is (= 200 s2))
      (test/is (= 200 s3))
      (test/is (= 200 s4))
      (test/is (= 6 (count b1)))
      (test/is (= 5 (count b2)))
      (test/is (= 1 (count b3)))
      (test/is (= 0 (count b4))))))
