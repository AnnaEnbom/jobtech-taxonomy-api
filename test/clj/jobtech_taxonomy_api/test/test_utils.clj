(ns jobtech-taxonomy-api.test.test-utils
  (:require
   [clojure.string :as str]
   [jobtech-taxonomy-api.authentication-service :as auth-service]
   [jobtech-taxonomy-api.config :as config]
   [jobtech-taxonomy-api.db.database-connection :as conn]
   [jobtech-taxonomy-api.handler :as handler]
   [jobtech-taxonomy-api.middleware.formats :as formats]
   [jobtech-taxonomy-api.test.db.database :as db]
   [mount.core :as mount]
   [muuntaja.core :as m]
   [ring.mock.request :as mock])
  (:import (java.util UUID)))

(defn header-auth-admin [] {:key "api-key", :val (auth-service/get-token :admin)})

(defmacro with-properties
  "Run a badly simulated closure with a system property. Not thread safe."
  [property-map & body]
  `(let [pm# ~property-map
         props# (into {} (for [[k# v#] pm#]
                           [k# (System/getProperty k#)]))]
     (doseq [k# (keys pm#)]
       (System/setProperty k# (get pm# k#)))
     (try
       ~@body
       (finally
         (doseq [k# (keys pm#)]
           (if-not (get props# k#)
             (System/clearProperty k#)
             (System/setProperty k# (get props# k#))))))))

(defn fixture
  "Setup a temporary database, run (f), and then teardown the database."
  [f]
  (let [config (update (config/make-config)
                       :datomic-name str "-" (UUID/randomUUID))]
    (with-properties {"integration-test-db" (:datomic-name config)}

      (try
        (db/init-database config)

        (println (mount/start #'config/env
                              #'conn/conn
                              #'handler/app))

        (f)

        (println (mount/stop #'conn/conn
                             #'config/env
                             #'handler/app))

        (finally
          (db/delete-database config))))))

(defn parse-json [body]
  (m/decode formats/instance "application/json" body))

(defn make-req [req [first & rest]]
  (if (nil? first)
    req
    (make-req (mock/header req (get first :key) (get first :val)) rest)))

(defn make-request [method endpoint & {:keys [headers query-params]}]
  (let [req (mock/request method endpoint)
        req-w-headers (if headers
                        (make-req req headers)
                        req)]
    (if query-params
      (mock/query-string req-w-headers
                         (str/join "&" (map #(str/join "=" (list (get % :key) (get % :val))) query-params)))
      req-w-headers)))

(defn send-request [method endpoint & {:keys [headers query-params]}]
  (let [req (make-request method endpoint :headers headers, :query-params query-params)]
    (handler/app req)))

(defn send-request-to-json-service [method endpoint & {:keys [headers query-params]}]
  (let [response (send-request method endpoint :headers headers, :query-params query-params)
        status (:status response)
        body (parse-json (:body response))]
    (list status body)))
