(ns ^:integration-concepts-tests jobtech-taxonomy-api.test.concepts-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.core :as core]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.test.db.database :as db]
            [jobtech-taxonomy-api.test.test-utils :as util]))

(test/use-fixtures :each util/fixture)

(deftest ^:integration-concepts-test-no concepts-test-no-params
  (testing "test query without parameters"
    (concepts/assert-concept "Stallman" {:type "skill" :definition "cyklade" :preferred-label "cykla"})
    (let [_ (db/create-version-0)
          [status body] (util/send-request-to-json-service
                         :get "/v1/taxonomy/main/concepts"
                         :headers [(util/header-auth-admin)]
                         :query-params [])
          _ (versions/create-new-version 1)]
      (test/is (= 200 status)))))

(deftest ^:integration-concepts-test-0 concepts-test-0
  (testing "test assert concept"
    (concepts/assert-concept "Stallman" {:type "skill" :definition "cyklade" :preferred-label "cykla"})
    (let [[status body] (util/send-request-to-json-service
                         :get "/v1/taxonomy/main/concepts"
                         :query-params [{:key "type", :val "skill"}])
          found-concept (first (concepts/find-concepts {:preferred-label "cykla" :version :next}))]
      (is (= "cykla" (get found-concept :concept/preferred-label))))))

(deftest ^:integration-concepts-test-1 concepts-test-1
  (testing "test concept relation 'related'"
    (let [id-1 (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                            :definition "pertest0"
                                                            :preferred-label "pertest0"})
                       [2 :concept/id])
          id-2 (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                            :definition "pertest1"
                                                            :preferred-label "pertest1"})
                       [2 :concept/id])
          [tx rel] (concepts/assert-relation "Stallman" nil id-1 id-2 "related" "desc" 0)
          rel (concepts/fetch-relation-entity-id-from-concept-ids-and-relation-type id-1 id-2 "related")]
      (is (some? rel)))))

(deftest replacement-deprecates-old-concept
  (let [writing-id (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                                :definition "writing"
                                                                :preferred-label "Writing"})
                           [2 :concept/id])
        texting-id (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                                :definition "texting"
                                                                :preferred-label "Texting"})
                           [2 :concept/id])
        _ (core/replace-deprecated-concept "Stallman" writing-id texting-id nil)
        deprecated-writing (first (concepts/find-concepts {:id writing-id :version :next :deprecated true}))]
    (is (:concept/deprecated deprecated-writing))
    (is (= texting-id (-> deprecated-writing :concept/replaced-by first :concept/id)))))

;; Define desired behaviour of the include-deprecated parameter
(deftest include-deprecated
  (let [;; Create two concepts
        deprecated-job-id (get-in (concepts/assert-concept "user0" {:type "occupation-name"
                                                                    :definition "deprecated-job"
                                                                    :preferred-label "deprecated-job"})
                                  [2 :concept/id])
        undeprecated-job-id (get-in (concepts/assert-concept "user0" {:type "occupation-name"
                                                                      :definition "undeprecated-job"
                                                                      :preferred-label "undeprecated-job"})
                                    [2 :concept/id])

        ;; Deprecate one of the two created concepts
        _ (core/retract-concept "user0" deprecated-job-id "there are no kusks anymore")

        ;; Query the DB with deprecated=[true|false|unspecified]
        dep-true (map #(get % :concept/id)
                      (concepts/find-concepts {:type ["occupation-name"] :version :next :deprecated true}))
        dep-false (map #(get % :concept/id)
                       (concepts/find-concepts {:type ["occupation-name"] :version :next :deprecated false}))
        dep-unspec (map #(get % :concept/id)
                        (concepts/find-concepts {:type ["occupation-name"] :version :next}))

        ;; Query the API with deprecated=[true|false|unspecified]
        [_ body-api-dep-true] (util/send-request-to-json-service
                               :get "/v1/taxonomy/main/concepts"
                               :headers [(util/header-auth-admin)]
                               :query-params [{:key "type", :val "occupation-name"}
                                              {:key "deprecated" :val "true"}
                                              {:key "version" :val "next"}])
        [_ body-api-dep-false] (util/send-request-to-json-service
                                :get "/v1/taxonomy/main/concepts"
                                :headers [(util/header-auth-admin)]
                                :query-params [{:key "type", :val "occupation-name"}
                                               {:key "deprecated" :val "false"}
                                               {:key "version" :val "next"}])
        [_ body-api-dep-unspec] (util/send-request-to-json-service
                                 :get "/v1/taxonomy/main/concepts"
                                 :headers [(util/header-auth-admin)]
                                 :query-params [{:key "type", :val "occupation-name"}
                                                {:key "version" :val "next"}])

        api-dep-true (map #(get % :taxonomy/id) body-api-dep-true)
        api-dep-false (map #(get % :taxonomy/id) body-api-dep-false)
        api-dep-unspec (map #(get % :taxonomy/id) body-api-dep-unspec)]

    ;; If deprecated=true, we should see both concepts
    (is (= (set dep-true) (set [undeprecated-job-id deprecated-job-id])))
    ;; If deprecated=false, we should only see the undeprecated concept
    (is (= (set dep-false) (set [undeprecated-job-id])))
    ;; If deprecated is unspecifiedf, we should get the same results as deprecated=false
    (is (= (set dep-unspec) (set dep-false)))

    ;; If deprecated=true, we should see both concepts
    (is (= (set api-dep-true) (set [undeprecated-job-id deprecated-job-id])))
    ;; If deprecated=false, we should only see the undeprecated concept
    (is (= (set api-dep-false) (set [undeprecated-job-id])))
    ;; If deprecated is unspecifiedf, we should get the same results as deprecated=false
    (is (= (set api-dep-unspec) (set api-dep-false)))))
