(ns ^:integration-graphql-tests jobtech-taxonomy-api.test.graphql-test
  (:require
   [clojure.test :as test]
   [jobtech-taxonomy-api.db.concepts :as concepts]
   [jobtech-taxonomy-api.db.versions :as versions]
   [jobtech-taxonomy-api.test.db.database :as db]
   [jobtech-taxonomy-api.test.test-utils :as util]))

(test/use-fixtures :each util/fixture)

(def ^:private broaderSkill {:type "skill" :definition "Drive machinery" :preferred-label "drive"})
(def ^:private narrowerSkill1 {:type "skill" :definition "ride bikes" :preferred-label "bike"})
(def ^:private narrowerSkill2 {:type "skill" :definition "fly airplanes" :preferred-label "fly"})

(def ^:private userid "Stallman")
(def ^:private endpoint "/v1/taxonomy/graphql")

(def schemaQuery
  (str
   "query IntrospectionQuery {"
   "__schema {"
   "queryType { name }"
   "}}"))

(test/deftest ^:integration-graphql-test-0 access-schema-without-auth
  (test/testing "test that the schema is accessible without authentication"
    (let [[status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val schemaQuery}])]
      (test/is (= 200 status)))))

(test/deftest ^:integration-graphql-test-1 graphql-test-1
  (test/testing "test simple graphql query"
    (let [_ (db/create-version-0)
          [_ _ new-concept] (concepts/assert-concept userid broaderSkill)
          query (str "{concepts(id: \"" (:concept/id new-concept) "\") {preferred_label}}")
          _ (versions/create-new-version 1)
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val query}])
          preferred_label (get-in body [:data :concepts 0 :preferred_label])]
      (test/is (= 200 status))
      (test/is (= "drive" preferred_label)))))

(defn- buildRelationQuery
  [relation id id1 id2]
  (str
   "{concepts(id: \"" id "\") {"
   relation "(id: [\"" id1 "\" \"" id2 "\"]) {"
   "preferred_label"
   "}}}"))

(defn- test-relation
  [relation]
  (let [_ (db/create-version-0)
        [_ _ broaderConcept] (concepts/assert-concept userid broaderSkill)
        [_ _ narrowerConcept1] (concepts/assert-concept userid narrowerSkill1)
        [_ _ narrowerConcept2] (concepts/assert-concept userid narrowerSkill2)
        id (:concept/id broaderConcept)
        id1 (:concept/id narrowerConcept1)
        id2 (:concept/id narrowerConcept2)
        _ (concepts/assert-relation
           userid nil id id1 relation "desc" 0)
        _ (concepts/assert-relation
           userid nil id id2 relation "desc" 0)
        _ (versions/create-new-version 1)
        query (buildRelationQuery relation id id1 id2)
        [status body] (util/send-request-to-json-service
                       :get endpoint
                       :query-params [{:key "query", :val query}])
        rel (case relation
              "narrower" :narrower
              "broader" :broader
              "related" :related
              (throw (Exception. (str "'" relation "' not supported in test-relation"))))
        preferred_label1 (get-in body [:data :concepts 0 rel 0 :preferred_label])
        preferred_label2 (get-in body [:data :concepts 0 rel 1 :preferred_label])]
    (test/is (= 200 status))
    (test/is (= ["bike" "fly"] (sort [preferred_label1 preferred_label2])))))

(test/deftest ^:integration-graphql-test-2 graphql-test-2
  (test/testing "test a 'broader' graphql query"
    (test-relation "broader")))

(test/deftest ^:integration-graphql-test-3 graphql-test-3
  (test/testing "test a 'related' graphql query"
    (test-relation "related")))

(test/deftest ^:integration-graphql-test-4 graphql-test-4
  (test/testing "test adding a broader relation and find it by a narrower graphql query"
    (let [_ (db/create-version-0)
          [_ _ broaderConcept] (concepts/assert-concept userid broaderSkill)
          [_ _ narrowerConcept] (concepts/assert-concept userid narrowerSkill1)
          id (:concept/id broaderConcept)
          id1 (:concept/id narrowerConcept)
          _ (concepts/assert-relation
             userid nil id1 id "broader" "desc" 0)
          _ (versions/create-new-version 1)
          query (buildRelationQuery "narrower" id id1 "incorrect conceptID")
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val query}])
          preferred_label (get-in body [:data :concepts 0 :narrower 0 :preferred_label])]
      (test/is (= 200 status))
      (test/is (= "bike" preferred_label)))))
