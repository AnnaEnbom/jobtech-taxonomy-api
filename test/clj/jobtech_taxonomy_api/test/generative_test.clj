(ns ^:integration-generative-tests jobtech-taxonomy-api.test.generative-test
  (:require
   [clojure.set :as set]
   [clojure.spec.alpha :as spec]
   [clojure.spec.gen.alpha :as gen]
   [clojure.test :as test]
   [jobtech-taxonomy-api.db.concepts :as concepts]
   [jobtech-taxonomy-api.db.versions :as versions]
   [jobtech-taxonomy-api.test.db.database :as db]
   [jobtech-taxonomy-api.test.test-utils :as util]))

(test/use-fixtures :each util/fixture)

(spec/def ::non-empty-string?
  (spec/and string?
            #(not= 0 (count %))))

;; define a concept generator
(spec/def ::type
  (spec/or ::random ::non-empty-string?
           ::collection #{"skill" "language" "keyword"}))
(spec/def ::definition
  (spec/or ::random ::non-empty-string?
           ::collection #{"programming" "digging" "driving"}))
(spec/def ::preferred-label
  (spec/or ::random ::non-empty-string?
           ::collection #{"programmer" "digger" "driver"}))
(spec/def ::concept
  (spec/keys :req [::type ::definition ::preferred-label]))

;; define a concept query
(spec/def ::id
  (spec/or ::random ::non-empty-string?
           ::collection #{"rEeE_DXA_Q4h" "zRtu_fYj_XC4" "mHpA_1Zm_7TP"}))
(spec/def ::include-deprecated boolean?)
(spec/def ::deprecated boolean?)
(spec/def ::relation
  (spec/or ::random ::non-empty-string?
           ::collection #{"broader" "narrower" "related" "substitutability-to" "substitutability-from"}))
(spec/def ::related-ids (spec/coll-of ::id))
(spec/def ::offset
  (spec/and int?
            #(>= % 0)))
(spec/def ::limit
  (spec/and int?
            #(> % 0)))
(spec/def ::version
  (spec/and int?
            #(>= % 0)
            #(< % 3)))
(spec/def ::include-legacy-information boolean?)
(spec/def ::concept-query-parameters
  (spec/keys :opt
             [::id ::preferred-label
              ::type ::include-deprecated
              ::deprecated ::relation
              ::related-ids ::offset
              ::limit ::version
              ::include-legacy-information]))

;; Convert the randomly generated data to:
;;  :query-params [{:key "type", :val "occupation-name"}
;;                 {:key "deprecated" :val "false"}
;;                 {:key "version" :val "next"})))
(defn adapt-concept-query [m]
  (mapv (fn [[k v]] {:key (name k) :val (str v)}) (seq m)))

(defn generate [spec]
  (gen/generate (spec/gen spec)))

(defn clear-namespace [m]
  (set/rename-keys m
                   (zipmap (map (fn [k] k) (keys m))
                           (map (fn [k] (keyword (name k))) (keys m)))))

;; Assert nbr generated concepts to the database
(defn assert-concepts [nbr]
  (let [concepts (map (fn [_] (clear-namespace (generate ::concept))) (repeat nbr 0))
        _ (doall (map (fn [c] (concepts/assert-concept "Stallman" c)) concepts))]
    concepts))

(defn send-concept-request [p]
  (let [params (adapt-concept-query (clear-namespace p))]
    (util/send-request-to-json-service
     :get "/v1/taxonomy/main/concepts"
     :query-params params)))

(spec/fdef send-concept-request
  :args (spec/cat :spec ::concept-query-parameters)
  :ret [(spec/and
         int?
         #(= % 200))
        string?])

(test/deftest ^:integration-generative-concepts generative-test-concepts
  (test/testing "test query with generated parameters"
    (let [_ (db/create-version-0)
          _ (assert-concepts (rand-int 10))
          _ (versions/create-new-version 1)
          _ (assert-concepts (rand-int 10))
          _ (versions/create-new-version 2)
          res (doall (spec/exercise-fn `send-concept-request 10))]
      (doall (map #(test/is (= 200 (first (second %)))) res)))))

(defmacro int-list [len]
  `(spec/and
    (spec/coll-of
     (spec/int-in 1 ~len))
    #(< (count %) ~len)
    #(> (count %) 0)))

;; Generate a concept query used for the pagination test
(spec/def ::concept-paging-query
  (spec/or
   :type (spec/keys :req [::type])
   :definition (spec/keys :req [::definition])
   :preferred_label (spec/keys :req [::preferred-label])))

;; Split up [0, len] randomly into sections, e.g. ([0..x] [x..y] [y..len])
(defn get-paging [len]
  (let [quantified (dedupe (sort (generate (int-list len))))
        lst1 (cons 0 quantified)
        lst2 (sort (conj quantified len))]
    (map (fn [from to] [from (- to from)]) lst1 lst2)))

;; Create paging queries
(defn get-paging-queries [len params]
  (let [base_params (clear-namespace params)
        pages (get-paging len)]
    (mapv #(adapt-concept-query (merge base_params {:offset (first %) :limit (second %)})) pages)))

;; Define the input and output of the tested function
(spec/fdef test-concept-paging
  :args (spec/cat :query ::concept-paging-query)
  :ret (spec/and
        boolean?
        #(= true %)))

;; Send a main/concepts request and return the body
(defn send-concept-request-body [params]
  (let [[status body]
        (util/send-request-to-json-service
         :get "/v1/taxonomy/main/concepts"
         :query-params params)
        _ (println (pr-str "body:" body))
        _ (test/is (= 200 status))]
    body))

 ;; Return true if the test passed.
 ;;
 ;; The test first runs the question without paging, then with,
 ;; and compares the results.
(defn test-concept-paging [params]
  (let [p (adapt-concept-query (clear-namespace params))
        _ (println (pr-str "before golden request:"))
        body (send-concept-request-body p)
        len (count body)]
    (if (= 0 len)
      true ;; return true for now
      (let [pagingQueries (get-paging-queries len params)
            _ (println (pr-str "paging query:" pagingQueries))
            _ (println (pr-str "before concat requests:"))
            [bd] (doall
                  (concat (map #(send-concept-request-body %) pagingQueries)))
            _ (println (pr-str "golden:" body))
            _ (println (pr-str "concat:" bd))]
        (= body bd)))))

(test/deftest ^:integration-generative-concepts-paging
  generative-test-concepts-paging
  (test/testing "test main/concepts paging generatively"
    (let [_ (db/create-version-0)
          concepts (assert-concepts (rand-int 40))
          _ (versions/create-new-version 1)
          res (spec/exercise-fn `test-concept-paging 10)]
      (doall (map #(test/is (= true (second %))) res)))))
