(ns ^:integration-concepts-tests jobtech-taxonomy-api.test.concept-private-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.test.test-utils :as util]))

;; Don't forget to (start) the server when developing
;;(use 'kaocha.repl)(run 'jobtech-taxonomy-api.test.changes-test/changes-test-0)


(test/use-fixtures :each util/fixture)

(defn create-new-concept-via-api [{:keys [type definition preferred-label alternative-labels hidden-labels]}]
  (util/send-request-to-json-service
   :post "/v1/taxonomy/private/concept"
   :headers [(util/header-auth-admin)]
   :query-params [{:key "type", :val type}
                  {:key "definition" :val definition}
                  {:key "preferred-label" :val preferred-label}
                  {:key "alternative-labels" :val alternative-labels}
                  {:key "hidden-labels" :val hidden-labels}]))

(defn update-concept-via-api [{:keys [id
                                      type
                                      preferred-label
                                      definition
                                      alternative-labels
                                      hidden-labels]}]
  (let [args (cond-> [{:key "id", :val id}]
               type (conj {:key "type", :val type})
               definition (conj {:key "definition" :val definition})
               preferred-label (conj {:key "preferred-label" :val preferred-label})
               alternative-labels (conj {:key "alternative-labels" :val alternative-labels})
               hidden-labels (conj {:key "hidden-labels" :val hidden-labels}))]
    (util/send-request-to-json-service
     :patch "/v1/taxonomy/private/accumulate-concept"
     :headers [(util/header-auth-admin)]
     :query-params args)))

(deftest ^:integration-concept-private-test-0 concepts-private-test-0
  (testing "test assert concept"
    (let [label (gensym "potatisodling-")
          [status body] (create-new-concept-via-api {:type "skill"
                                                     :definition label
                                                     :preferred-label label
                                                     :alternative-labels "a | b | c "
                                                     :hidden-labels " a|"})

          id (:concept/id (:concept body))
          created-concept (first (concepts/find-concepts {:id id :version :next}))
         ;; _ (clojure.pprint/pprint "--------------------------created concept -------------------------")
         ;; _ (clojure.pprint/pprint created-concept)
          _ (update-concept-via-api {:id id
                                     :hidden-labels "new-hidden-label-1"
                                     :alternative-labels "new-alternative-label-1|new-alternative-label-2"})

          updated-concept (first (concepts/find-concepts {:id id :version :next}))
;;          _ (clojure.pprint/pprint "--------------------------updated concept -------------------------")
          ;;        _ (clojure.pprint/pprint updated-concept)
          ]
      (is (= ["a" "b" "c" "new-alternative-label-1" "new-alternative-label-2"]
             (:concept/alternative-labels updated-concept)))
      (is (= ["a" "new-hidden-label-1"]
             (:concept/hidden-labels updated-concept))))))
