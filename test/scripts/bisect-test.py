#!/usr/bin/env python3

#
# This script can be used when running git bisect against a running server.
# Start by copying the script to the root folder. Otherwise bisect will not find it when checking out an
# older version.
# cp test/scripts/bisect-test.py .
#
# Start a bisect run with:
# git bisect start <failing commit> <passing commit>
#    e.g. git bisect start 5a2f5e0bbbcee298ad73666eecb4a4f8d1214cf8 69aa1a887629d59dcfb8f406262483276d3b4b41
# git bisect run ./bisect-test.py
# git bisect reset # quit the bisect session
#
# Don't use this script if the error can be triggered with the kaocha test.
#

import atexit
import sys
from subprocess import Popen, run, PIPE
import signal
import psutil
import time
import os

# Config test
test_endpoint = "http://localhost:3000/v1/taxonomy/private/concept/automatic-daynotes/?id=ANyE_2x5_dFg"
api_key = "222"

# If set to false git bisect will ignore this version if the server does not start
config_build_fail_is_failure = False

# Patch the bisect version by setting the database to Datomic.
config_patch = False

# File that wget writes to
wgetOutputFile = "wget-output.html"

def terminate_children(pid):
    try:
        parent = psutil.Process(pid)
    except psutil.NoSuchProcess:
        print("no such process")
        return
    children = parent.children(recursive=True)
    for process in children:
        process.send_signal(signal.SIGKILL)
        terminate_children(process.pid)
    parent.send_signal(signal.SIGKILL)


def patch():
        print("patch")
        returnCode = 0
        try:
            res = run(
                [
                    "sed",
                    "-i.old",
                    "s/def\ db-backend\ :datahike/def\ db-backend\ :datomic/",
                    "src/clj/jobtech_taxonomy_api/config.clj",
                ],
                stdout=PIPE,
            )
            res = run(
                [
                    "sed",
                    "-i.old",
                    "s/sendandi.api/datomic.client.api/ ; s/wanderung.db.api/datomic.client.api/",
                    "src/clj/jobtech_taxonomy_api/db/database_connection.clj",
                ],
                stdout=PIPE,
            )
            res = run(
                [
                    "sed",
                    "-i.old",
                    "s/.*datahike-client.*datahike-cfg.*// ; s/;\ (d\/client/(d\/client/",
                    "src/clj/jobtech_taxonomy_api/db/database_connection.clj",
                ],
                stdout=PIPE,
            )
            returnCode = res.returncode
        except CalledProcessError as e:
            output = e.output
            returncode = e.returncode

        return 0

def unpatch():
        print("unpatch")
        returnCode = 0
        try:
            res = run(
                [
                    "git",
                    "checkout",
                    ".",
                ],
                stdout=PIPE,
            )
            returnCode = res.returncode
        except CalledProcessError as e:
            output = e.output
            returncode = e.returncode

        return returnCode

def ping_server():
    for i in range(10):
        print("pinging server")
        returnCode = 0
        try:
            res = run(
                [
                    "wget",
                    "-q",
                    "http://localhost:3000/v1/taxonomy/main/versions",
                    "-O",
                    wgetOutputFile,
                    "--header",
                    "accept: application/json",
                    "--header",
                    "api-key: 111",
                ],
                stdout=PIPE,
            )
            returnCode = res.returncode
        except CalledProcessError as e:
            output = e.output
            returncode = e.returncode

        if returnCode == 0:
            return 0
        time.sleep(1)
    # Failed reaching server
    return 1


def start_server():
    # Check if bisect version should be patched before starting server
    if config_patch:
        res = patch()
        if res != 0:
            sys.exit(1)

    p = Popen(["clj", "-A:dev", "-X", "jobtech-taxonomy-api.core/start", ":port", "3000"], stdout=PIPE, stderr=PIPE)

    # Register this process for termination at exit
    atexit.register(terminate_children, p.pid)

    failed = False
    for line in iter(p.stdout.readline, ""):
        line = line.decode()
        print(line, end="")
        if "starting HTTP server on port 3000" in line:
            failed = False
            break
        elif ("Exception" in line) or ("server failed to start" in line):
            failed = True
            break
            print(line, end="")

    if failed:
        print("Failed to start server")
        if config_build_fail_is_failure:
            return 1
        else:
            print("Server failed, but skipping commit")
            return 125 # defined in git bisect
    else:
        print("Server started")
        return 0


def run_test():
    returnCode = 0
    try:
        res = run(
            [
                "wget",
                "-q",
                "-O",
                wgetOutputFile,
                test_endpoint,
                "--header",
                "accept: application/json",
                "--header",
                "api-key: " + api_key,
            ],
            stdout=PIPE,
        )
        returnCode = res.returncode
    except CalledProcessError as e:
        output = e.output
        returncode = e.returncode
        print("Got exception with " + str(returnCode))

    if returnCode != 0:
        print("test failed with code " + str(returnCode))
    return returnCode


res = start_server()
if res == 0:
    res = ping_server()
    if res != 0:
        print("Failed reach server")
        sys.exit(res)
    else:
        print("Server up")

    # Run tests
    print("Run test")
    res = run_test()

# Remove the patch added before starting the server
if config_patch:
    unpatch()

# Remove wget file, comment if you want to save it
os.remove(wgetOutputFile)

print("Stopping server")
# Stopping the server is handled in start_server

sys.exit(res)
