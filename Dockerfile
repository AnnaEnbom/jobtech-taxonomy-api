FROM openjdk:11.0.11-jdk-slim as builder

ENV CLOJURE_VERSION=1.10.3.855

RUN apt-get update \
 && apt-get install -y curl git \
 && curl -L -o install.sh https://download.clojure.org/install/linux-install-$CLOJURE_VERSION.sh \
 && chmod +x install.sh \
 && ./install.sh

COPY deps.edn .

RUN clojure -A:prod -P
RUN clojure -A:uberjar -P

COPY . .

RUN clojure -X:uberjar :jar '"app.jar"'

FROM openjdk:11.0.11-jdk-slim

COPY --from=builder app.jar app.jar

EXPOSE 3000

CMD java -jar app.jar
