# GraphQL Examples


Fetch tree of occupation-field -> ssyk-level-4 -> occupation-name in version 1

```graphql
query MyQuery {
  concepts(type: "occupation-field", version: "1") {
    id
    preferred_label
    type
    narrower (type: "ssyk-level-4"){
      id
      preferred_label
      type
      narrower (type: "occupation-name") {
        id
        preferred_label
        type
      }
    }
    
  }
}
```