import nltk
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
import pickle

#
# Creates a frequency dictionary of a corpus
#

# Download packages
nltk.download('punkt')
nltk.download('stopwords')

stop_words = stopwords.words('swedish')
porter = PorterStemmer()
counts = dict()

# opening the text file
with open('alltext.txt','r') as file:
    # read each line
    for line in file:
        # handle each line
        for word in word_tokenize(line):
            if word.isalpha() and word not in stop_words:
                w = porter.stem(word).lower()
                counts[w] = counts.get(w, 0) + 1

# Save dictionary to file
f = open("stemmed.pkl","wb")
pickle.dump(counts, f)
f.close()
