import json
import nltk
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
import pickle
import pandas as pd

#
# Calculate the popularity of a concept based on the preferred label
#

# Download packages
nltk.download('punkt')
nltk.download('stopwords')

# Init tokenizer and stemmer
stop_words = stopwords.words('swedish')
porter = PorterStemmer()

def mytokenize(text):
    # split into words
    tokens =  word_tokenize(text.replace("/", " "))

    # Stem, lowercase, and remove all tokens that are not alphanumeric
    words = [porter.stem(word).lower() for word in tokens if word.isalpha() and word not in stop_words]
    return words

def load_frequencies():
    return pd.read_pickle(r'stemmed.pkl')

def load_concepts():
    # read file
    with open('concepts.json', 'r') as myfile:
        data=myfile.read()

    # parse file
    return json.loads(data)

# Get concepts
concepts = load_concepts()

# Load word frequencies
freq = load_frequencies()

# Loop through all concepts
output_json = {}
for c in concepts:
    cid = c['id']
    label = c['preferred_label']
    tok = mytokenize(label)
    freqSum = 0
    for l in tok:
        freqSum += freq.get(l, 0)
    mean = 0 if len(tok) == 0 else freqSum/len(tok)
    if int(mean) > 0:
        output_json[cid] = int(mean)

with open('freq_data.json', 'w') as outfile:
    json.dump(output_json, outfile)
