(ns jobtech-taxonomy-api.middleware.misc
  (:require
   [clojure.string :as str]
   [clojure.tools.logging :as log]))

(defn log-request [handler]
  (fn [request]
    (let [api-key (get-in request [:headers "api-key"])
          referer (get-in request [:headers "referer"])
          from (get-in request [:headers "from"])
          req (vals (select-keys request [:request-method :uri :query-params]))
          _ (log/info
             (str "request: " req
                  (if referer (str " referer: " referer) "")
                  (if from (str " from: " from) "")
                  (if api-key (str " api-key: " api-key) "")))]
      (handler request))))

(defn trim-request-params [handler]
  (fn [request]
    (let [req (reduce
               #(update-in %1 [:query-params %2] str/trim)
               request
               (keys (get request :query-params)))]
      (handler req))))
