(ns jobtech-taxonomy-api.db.popularity
  (:require [clj-json.core :as json]
            [clojure.java.io :as io])
  (:import (java.io BufferedReader StringReader)))

(defn- fetch-popularity-helper []
  (first
   (json/parsed-seq
    (io/reader (io/resource "popularity/freq_data.json")))))

(def fetch-popularity
  (memoize fetch-popularity-helper))
