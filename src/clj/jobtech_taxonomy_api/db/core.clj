(ns jobtech-taxonomy-api.db.core
  (:require
   [clojure.tools.logging :as log]
   [jobtech-taxonomy-api.db.api-util :as api-util]
   [jobtech-taxonomy-api.db.concepts :as concepts]
   [jobtech-taxonomy-api.db.database-connection :as db]))

(defn retract-concept [user-id id comment]
  (when (concepts/exists? (db/get-db) id)
    (let [result (db/transact (db/get-conn) {:tx-data [(api-util/user-action-tx user-id comment)
                                                       {:concept/id id
                                                        :concept/deprecated true}]})]
      (log/info result)
      result)))

(def get-relation-types-query
  '[:find ?v :where [_ :relation/type ?v]])

(defn get-relation-types []
  (->> (db/q get-relation-types-query (db/get-db))
       (sort-by first)
       (apply concat)))

;; TODO appeda pa replaced by listan

(defn replace-deprecated-concept [user-id old-concept-id new-concept-id comment]
  (let [db (db/get-db)]
    (when (and (concepts/exists? db old-concept-id)
               (concepts/exists? db new-concept-id))
      (db/transact (db/get-conn) {:tx-data [(api-util/user-action-tx user-id comment)
                                            {:concept/id old-concept-id
                                             :concept/deprecated true
                                             :concept/replaced-by [[:concept/id new-concept-id]]}]}))))

(defn unreplace-deprecated-concept [user-id old-concept-id new-concept-id comment]
  (let [db (db/get-db)]
    (when (and (concepts/exists? db old-concept-id)
               (concepts/exists? db new-concept-id))
      (db/transact (db/get-conn) {:tx-data [(api-util/user-action-tx user-id comment)
                                            [:db/retract [:concept/id old-concept-id] :concept/replaced-by [:concept/id new-concept-id]]]}))))

(def get-all-taxonomy-types-query
  '[:find ?v :where [_ :concept/type ?v]])

(defn get-all-taxonomy-types "Return a list of taxonomy types." [version]
  (->> (db/q get-all-taxonomy-types-query (db/get-db version))
       (sort-by first)
       (flatten)
       (map name)))
