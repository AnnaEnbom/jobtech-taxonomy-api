(ns jobtech-taxonomy-api.db.database-connection
  (:require
   [clojure.string :as str]
   [jobtech-taxonomy-api.config :refer [env]]
   [datomic.client.api :as d]
   ; [sendandi.api :as d]
   [mount.core :refer [defstate]]))

;; Wrapping functions in the Datahike/Datomic interface
(def q d/q)
;; Use this db wrapper function to log queries
;(defn q
;  [& params]
;  (let [res (apply d/q params)
;        _ (println "query: " params "\nresp: " res "\n")]
;    res))

(defn- retry-helper [delay-ns nbr f & params]
  (try
    (apply f params)
    (catch Exception e
      (let [msg (ex-message e)]
        (cond
          (str/starts-with? msg "jobtech-taxonomy-api-retry: ")
          (throw e)
          (> nbr 5)
          (let [new-ex (Exception. (str "jobtech-taxonomy-api-retry: " msg))]
            (throw (doto new-ex (.setStackTrace (.getStackTrace e)))))
          :else
          (do
            (Thread/sleep (* nbr delay-ns))
            (println (str "Retry " nbr " caught exception: " msg))
            (apply retry-helper delay-ns (inc nbr) f params)))))))

;; Retry a function that throws an exception 5 times
(defn retry [delay-ns f & params]
  (apply retry-helper delay-ns 1 f params))

(def transact d/transact)
(def pull d/pull)
(def history d/history)
(def tx-range d/tx-range)
(def as-of d/as-of)
(def db d/db)

(defn get-client []
  ; (d/datahike-client (:datahike-cfg env)))
  (retry 1000 #(d/client (:datomic-cfg env))))

(defn get-conn []
  (retry 1000 #(d/connect (get-client) {:db-name (:datomic-name env)})))

(defstate ^{:on-reload :noop} conn
  :start (do (println "start:conn" (:datomic-name env))
             (get-conn)))

(def get-database-time-point-by-version-query
  '[:find ?tx
    :in $ ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(defn get-transaction-id-from-version [db version]
  (ffirst (d/q get-database-time-point-by-version-query db version)))

(def get-latest-released-version-query
  '[:find (max ?version)
    :in $
    :where [_ :taxonomy-version/id ?version]])

(defn get-latest-released-version [db]
  (ffirst (d/q get-latest-released-version-query db)))

;; This cannot use the conn var, as it will destroy the
;; integration tests (where the conn is made before the
;; tests fill the databases with test data).
(defn get-db
  "Get database, possibly at a point in time of some release.

  Optional version arg can be:
  - `:next` (default) - the latest database, includes unpublished changes
  - `:latest` - the database at a latest release time point
  - an int - the database at a time point of that release"
  ([]
   (get-db :next))
  ([version]
   (let [db (d/db (get-conn))]
     (cond
       (= :next version)
       db

       (= :latest version)
       (->> (or (get-latest-released-version db)
                (throw (ex-info "There are no releases yet"
                                {:db db})))
            (get-transaction-id-from-version db)
            (d/as-of db))

       (int? version)
       (d/as-of db (or (get-transaction-id-from-version db version)
                       (throw (ex-info "There is no such db version"
                                       {:version version
                                        :db db}))))

       :else
       (throw (ex-info "Invalid db version, use :latest, :next or int"
                       {:version version}))))))
