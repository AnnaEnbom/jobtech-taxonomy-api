(ns jobtech-taxonomy-api.db.api-util)

(defn parse-seach-concept-datomic-result [result]
  (map first result))

(defn pagination
  [offset limit coll]
  (cond->> coll
    offset (drop offset)
    limit (take limit)))

(def regex-char-esc-smap
  (let [esc-chars "(){}[]&^%$#!?*.+"]
    (zipmap esc-chars
            (map #(str "\\" %) esc-chars))))

(defn ignore-case [string]
  (str "(?iu:" string ")"))

(defn ignore-case-starts-with [string]
  (str "(?iu:" string ".*)"))

(defn ignore-case-contains [string]
  (str "(?iu:.*" string ".*)"))

(defn str-to-pattern-ignore-case
  [string]
  (->> string
       (replace regex-char-esc-smap)
       (reduce str)
       ignore-case))

(defn str-to-pattern-starts-with
  [string]
  (->> string
       (replace regex-char-esc-smap)
       (reduce str)
       ignore-case-starts-with))

(defn str-to-pattern-contains
  [string]
  (->> string
       (replace regex-char-esc-smap)
       (reduce str)
       ignore-case-contains))

(defn user-action-tx [user-id comment]
  (cond-> {:db/id "datomic.tx"
           :taxonomy-user/id user-id}
    comment (assoc :daynote/comment comment)))
