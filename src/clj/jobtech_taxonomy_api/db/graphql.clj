(ns jobtech-taxonomy-api.db.graphql
  (:require [clojure.instant :as inst]
            [clojure.main :as m]
            [clojure.string :as str]
            [com.walmartlabs.lacinia :as l]
            [com.walmartlabs.lacinia.resolve :as l.resolve]
            [com.walmartlabs.lacinia.schema :as l.schema]
            [jobtech-taxonomy-api.db.api-util :as api-util]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-api.middleware :as middleware]
            [jobtech-taxonomy-common.relation :as relation]
            [promesa.core :as promesa]
            [superlifter.api :as s]
            [superlifter.core :as superlifter]
            [urania.core :as urania])
  (:import (java.text SimpleDateFormat)
           (java.util TimeZone)))

;; GraphQL implementation on top of Lacinia. Lacinia does all the query parsing, while all we need to do is provide
;; data resolvers for declared fields. Data resolver is a function that takes 3 arguments:
;; - ctx — lacinia context map;
;; - args — query arguments for this field;
;; - value — value that we want to know more about.
;; It should return resolved value.
;; Queries on the root level receive nil value. Concept query fetches only very shallow concepts, containing only `:id`
;; key, and other fields like `:type` fetch concept types by that id.
;; This implementation uses Superlifter to batch fetches for fields of different concepts into single query, grouping
;; them by multi-resolver and its args. Multi-resolver is function similar to resolver, with difference being:
;; - 3rd argument is a list of values we want to know more about;
;; - it should return a list of resolved values.

(defn- sort-on-sort-order
  [l]
  (if (contains? (first l) :concept/sort-order)
    (sort-by :concept/sort-order l)
    l))

(defn- transform-concept-list
  "Sort a collection of maps according to key :sort-order"
  [reply]
  (sort-on-sort-order (map first reply)))

(defn- into-in [m & path+vals]
  (->> path+vals
       (partition 2)
       (reduce #(update-in %1 (first %2) into (second %2)) m)))

(defn- concepts [db {:keys [id type preferred_label_contains include_deprecated limit offset]}]
  (-> (db/q
       (cond-> {:query '{:find [(pull ?c [:concept/sort-order [:concept/id :as :id]])]
                         :in [$]
                         :where []}
                :args [db]}
         id (into-in [:args] [id]
                     [:query :in] '[[?id ...]]
                     [:query :where] '[[?c :concept/id ?id]])
         type (into-in [:args] [(if (vector? type) type [type])]
                       [:query :in] '[[?type ...]]
                       [:query :where] '[[?c :concept/type ?type]])
         preferred_label_contains (into-in [:args] [(api-util/str-to-pattern-contains preferred_label_contains)]
                                           [:query :in] '[?preferred_label_contains]
                                           [:query :where] '[[?c :concept/preferred-label ?preferred-label]
                                                             [(.matches ^String ?preferred-label ?preferred_label_contains)]])
         (not include_deprecated) (into-in [:query :where] '[[(ground false) ?deprecated]
                                                             [(get-else $ ?c :concept/deprecated false) ?deprecated]])
         limit (assoc :limit limit)
         offset (assoc :offset offset)
         true (into-in [:query :where] '[[?c :concept/id ?id]])))
      transform-concept-list))

(defn- inject-db
  "Resolver middleware that authenticates user and injects database for required version"
  [f]
  (fn [{::keys [api-key] :as ctx} {:keys [version] :as args} value]
    (cond
      (and (= "next" version) (not (middleware/authenticate-admin api-key)))
      (l.resolve/with-error [] {:message "Not authorized"})

      :else
      (let [version (or ({"latest" :latest "next" :next} version)
                        (Integer/parseInt version))
            db (db/get-db version)]
        (l.resolve/with-context (f (assoc ctx ::db db) args value)
          {::db db})))))

(defn- catch-multi-fetch-exceptions [fetch]
  (fn [ctx args values]
    (try
      (fetch ctx args values)
      (catch Exception e
        (promesa/resolved (repeat e))))))

(defn- fetcher [id fetch args]
  (with-meta (reify
               urania/DataSource
               (-identity [_] id)
               (-fetch [_ env]
                 (s/unwrap first ((catch-multi-fetch-exceptions fetch) env args [id])))
               urania/BatchedSource
               (-fetch-multi [this resources env]
                 (let [ids (map urania/-identity (cons this resources))]
                   (s/unwrap #(zipmap ids %) ((catch-multi-fetch-exceptions fetch) env args ids)))))
    ;; Urania batches received requests by `#(pr-str (type %))`. Some tools (like cider) override function's string
    ;; representation in a way that breaks this batching (they exclude identity hash code for readability). To make
    ;; batching reliable, we ensure proper string representation of this data source by explicitly including function's
    ;; identity hash code
    {:type [(.getName (class fetch)) (System/identityHashCode fetch) args]}))

(defn- batch-resolver
  "Resolver middleware that converts multi-resolver to a single field resolver

  When query is executed, it batches multiple requests to returned single resolver to a single request to the wrapped
  multi-resolver, thus solving N+1 problem"
  [fetch]
  (fn [ctx args value]
    (let [superlifter (::superlifter ctx)
          p (l.resolve/resolve-promise)]
      (-> superlifter
          (superlifter/enqueue! (fetcher value fetch args))
          (promesa/then (fn [result]
                          (if (instance? Exception result)
                            (l.resolve/deliver! p nil {:message (-> result Throwable->map m/ex-triage m/ex-str)})
                            (l.resolve/deliver! p result)))))
      p)))

(def fetch-concepts-from-db-query '[:find ?id ?v
                                    :in $ [?id ...] ?a
                                    :where
                                    [?c :concept/id ?id]
                                    [?c ?a ?v]])

(defn fetch-concepts-from-db [db ids attr]
  (db/q fetch-concepts-from-db-query db ids attr))

(defn- fetch-concepts-attr
  ([attr]
   (fetch-concepts-attr attr nil))
  ([attr default]
   (fn [ctx _ concepts]
     (let [db (::db ctx)
           ids (mapv :id concepts)
           id->value (into {} (fetch-concepts-from-db db ids attr))]
       (mapv #(id->value % default) ids)))))

(defn- fetch-concepts-list-attr [attr]
  (fn [ctx _ concepts]
    (let [db (::db ctx)
          ids (mapv :id concepts)
          id->value  (reduce
                      (fn [acc element] (update acc (first element) conj (second element))) {}
                      (fetch-concepts-from-db db ids attr))]
      (mapv #(id->value % []) ids))))

(defn- fetch-relation-attr
  [attr]
  (fn [ctx _ relations]
    (mapv attr relations)))

(defn- fetch-legacy-ids [ctx _ concepts]
  (let [db (::db ctx)
        ids (mapv :id concepts)
        id->value (into {} (db/q '[:find ?id ?legacy-id
                                   :in $ [?id ...]
                                   :where
                                   [?c :concept/id ?id]
                                   [?c :concept.external-database.ams-taxonomy-67/id ?legacy-id]]
                                 db ids))]
    (mapv id->value ids)))

(defn- make-relation-fetcher
  "Factory for relation multi-resolvers

  `enrich-query` that is expected to modify passed query to select related concept entities. It can expect `?c` being
  bound to source concept, and returned query needs to bind `?related-c`"
  [enrich-query]
  (fn [ctx {:keys [id type preferred_label_contains include_deprecated limit offset]} concepts]
    (let [db (::db ctx)
          ids (mapv :id concepts)
          datalog-q (-> {:query '{:find [?from-id ?id]
                                  :keys [from_id id]
                                  :in [$ % [?from-id ...]]
                                  :where [[?c :concept/id ?from-id]]}
                         :args [db relation/rules ids]}
                        enrich-query
                        (cond->
                         true (into-in [:query :where] '[[?related-c :concept/id ?id]])
                         type (into-in [:args] [(if (vector? type) type [type])]
                                       [:query :in] '[[?type ...]]
                                       [:query :where] '[[?related-c :concept/type ?type]])
                         id  (into-in [:args] [id]
                                      [:query :in] '[[?id-to ...]]
                                      [:query :where] '[[(= ?id ?id-to)]])
                         preferred_label_contains (into-in [:args] [(api-util/str-to-pattern-contains preferred_label_contains)]
                                                           [:query :in] '[?preferred_label_contains]
                                                           [:query :where] '[[?related-c :concept/preferred-label ?preferred-label]
                                                                             [(.matches ^String ?preferred-label ?preferred_label_contains)]])
                         (not include_deprecated) (into-in [:query :where] '[[(ground false) ?deprecated]
                                                                             [(get-else $ ?related-c :concept/deprecated false) ?deprecated]])
                         limit (assoc :limit limit)
                         offset (assoc :offset offset)))
          id->concepts (-> datalog-q
                           db/q
                           (->> (group-by :from_id)))]
      (mapv #(id->concepts % []) ids))))

(defn- edge-fetcher [relation-type relation-attrs]
  (make-relation-fetcher
    #(reduce
       (fn [acc field]
         (let [label (name field)
               field-var (symbol (str "?" label))]
           (-> acc
               (update-in [:query :where] conj ['?r field field-var])
               (update-in [:query :find] conj field-var)
               (update-in [:query :keys] conj (keyword (str/replace label \- \_))))))
       (update-in % [:query :where] conj (list 'edge '?c relation-type '?related-c '?r))
       relation-attrs)))

(def ^:private fetch-replaced-by
  (make-relation-fetcher
   #(into-in % [:query :where] '[[?c :concept/replaced-by ?related-c]])))

(def ^:private fetch-replaces
  (make-relation-fetcher
   #(into-in % [:query :where] '[[?related-c :concept/replaced-by ?c]])))

(defn- fetch-last-changed [ctx _ concepts]
  (let [db (::db ctx)
        ids (map :id concepts)
        id->concepts (into {} (db/q '[:find ?id (max ?inst)
                                      :in $ [?id ...]
                                      :where
                                      [?c :concept/id ?id]
                                      [?c _ _ ?tx]
                                      [?tx :db/txInstant ?inst]]
                                    db ids))]
    (mapv id->concepts ids)))

(defn- inject-superlifter
  "Resolver middleware that injects superlifter to execution context, allowing to use batch resolvers

  It also puts `::stop-superlifter` extension function that needs to be called after query is executed to shutdown
  superlifter fetch triggers"
  [f]
  (fn [ctx args value]
    (let [superlifter (superlifter/start!
                       {:buckets {:default {:triggers {:interval {:interval 50}}}}
                        :urania-opts {:env ctx}})]
      (-> (f (assoc ctx ::superlifter superlifter) args value)
          (l.resolve/with-context {::superlifter superlifter})
          (l.resolve/with-extensions assoc ::stop-superlifter #(superlifter/stop! superlifter))))))

(defn- catch-exceptions
  "Resolver middleware that catches exceptions and converts them to GraphQL errors"
  [f]
  (fn [ctx args value]
    (try
      (f ctx args value)
      (catch Exception e
        (l.resolve/with-error nil {:message (-> e Throwable->map m/ex-triage m/ex-str)})))))

(def ^:private ^ThreadLocal inst-format
  ;; SimpleDateFormat is not thread-safe, so we use a ThreadLocal proxy for access.
  ;; http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4228335
  (proxy [ThreadLocal] []
    (initialValue []
      (doto (SimpleDateFormat. "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        (.setTimeZone (TimeZone/getTimeZone "GMT"))))))

(def ^:private schema-concept-arguments
  {:id
   {:type '(list (non-null String))
    :description "Restrict results to these concept IDs"}
   :type
   {:type '(list (non-null String))
    :description "Restrict results to these concept types"}
   :preferred_label_contains
   {:type 'String
    :description "Filter on preferred label containing a string"}
   :limit
   {:type 'Int
    :description "Pagination: maximum amount of returned concepts"}
   :offset
   {:type 'Int
    :description "Pagination: skip this many returned concepts"}
   :include_deprecated
   {:type '(non-null Boolean)
    :description "Include deprecated concepts"
    :default-value false}})

(defn- deprecate-schema-field [str entity]
  (merge entity {:deprecated str}))

(defn- schema-field
  ([type description resolver]
   (schema-field type description resolver '{}))
  ([type description resolver args]
   {:type type
    :description description
    :args args
    :resolve resolver}))

(def ^:private resolve-concepts
  (catch-exceptions
   (inject-db
    (inject-superlifter
     (fn [ctx args _]
       (concepts (::db ctx) args))))))

(def ^:private relation-field-renames
  {"substitutability" "substitutes"})

(def ^:private relation-attrs-to-include
  {"substitutability" #{:relation/substitutability-percentage}})

(defn- relation-field-definitions []
  (->> relation/relations
       keys
       (map
         (fn [relation-type]
           (let [label (relation-field-renames relation-type relation-type)
                 attrs (or (relation-attrs-to-include relation-type)
                           (relation-attrs-to-include (relation/relations relation-type)))]
             [(keyword (str/replace label \- \_))
              (schema-field '(non-null (list (non-null :Concept)))
                            (str (str/capitalize label)
                                 " concepts"
                                 (when attrs
                                   (str
                                     " (results may include "
                                     (->> attrs
                                          (map #(str "`"
                                                     (-> % name (str/replace \- \_))
                                                     "`"))
                                          (str/join ","))
                                     ")")))
                            (batch-resolver (edge-fetcher relation-type attrs))
                            schema-concept-arguments)])))
       (into {})))

(defn- resolve-uri [_ _ {:keys [id]}]
  (str "http://data.jobtechdev.se/taxonomy/concept/" id))

(def ^:private schema
  (l.schema/compile
   {:queries {:concepts {:type '(non-null (list (non-null :Concept)))
                         :description "Fetch concepts"
                         :args (assoc schema-concept-arguments :version
                                      {:type '(non-null String)
                                       :description "Use this taxonomy version, valid values are `\"latest\"` for latest release, `\"next\"` for unpublished changes (requires admin rights) or number indicating the version"
                                       :default-value "latest"})
                         :resolve resolve-concepts}}
    :scalars {:Date {:description "A date.\n\nFormat: `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`"
                     :parse inst/read-instant-date
                     :serialize #(.format ^SimpleDateFormat (.get inst-format) %)}}
    :objects
    {:Concept
     {:description "Fundamental taxonomy type"
      :fields
      (merge
        {:id {:type '(non-null String)
              :description "ID of a concept"}
         :uri {:type '(non-null String)
               :description "URI of a concept"
               :resolve resolve-uri}
         :type
         (schema-field '(non-null String)
                       "Type of a concept"
                       (batch-resolver (fetch-concepts-attr :concept/type)))
         :preferred_label
         (schema-field '(non-null String)
                       "Textual name of a concept"
                       (batch-resolver (fetch-concepts-attr :concept/preferred-label)))
         :definition
         (schema-field '(non-null String)
                       "Concept definition"
                       (batch-resolver (fetch-concepts-attr :concept/definition)))
         :short_description
         (schema-field 'String
                       "A short description that is more informative than the preferred label"
                       (batch-resolver (fetch-concepts-attr :concept/short-description)))

         :alternative_labels
         (schema-field '(list String)
                       "Acronyms, abbreviations, spelling variants, and irregular plural/singular forms may be included among the alternative labels for a concept"
                       (batch-resolver (fetch-concepts-list-attr :concept/alternative-labels)))

         :hidden_labels
         (schema-field '(list String)
                       "A lexical label for a resource that should be hidden when generating visual displays of the resource, but should still be accessible to free text search operations"
                       (batch-resolver (fetch-concepts-list-attr :concept/hidden-labels)))

         :quality_level
         (schema-field 'Int
                       "quality level of this concept (1, 2 or 3)"
                       (batch-resolver (fetch-concepts-attr :concept/quality-level)))
         :sort_order
         (schema-field 'Int
                       "Concept sort-order"
                       (batch-resolver (fetch-concepts-attr :concept/sort-order)))
         :last_changed
         (schema-field :Date
                       "The date of the latest modification of a concept"
                       (batch-resolver fetch-last-changed))
         :ssyk_code_2012
         (schema-field 'String
                       "SSYK code that can be found on concepts of `ssyk-level-*` types"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/ssyk-code-2012)))
         :isco_code_08
         (schema-field 'String
                       "ISCO code that can be found on concepts of `isco-level-4` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/isco-code-08)))
         :esco_uri
         (schema-field 'String
                       "ESCO identifier URI that can be found on concepts of `isco-level-4` and `esco-occupation` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/esco-uri)))
         :eures_code_2014
         (schema-field 'String
                       "EURES code that can be found on concepts of `employment-duration` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/eures-code-2014)))
         :driving_licence_code_2013
         (schema-field 'String
                       "Driver licence code that can be found on concepts of `driving-licence` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/driving-licence-code-2013)))
         :nuts_level_3_code_2013
         (schema-field 'String
                       "NUTS level 3 code that can be found on concepts of `region` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/nuts-level-3-code-2013)))
         :national_nuts_level_3_code_2019
         (schema-field 'String
                       "Swedish Län code that can be found on concepts of `region` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/national-nuts-level-3-code-2019)))
         :nuts_level_3_code_2021
         (schema-field 'String
                       "NUTS level 3 code that can be found on concepts of `region` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/nuts-level-3-code-2021)))
         :lau_2_code_2015
         (schema-field 'String
                       "Swedish Municipality code that can be found on concepts of `municipality` type"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/lau-2-code-2015)))
         :iso_3166_1_alpha_2_2013
         (schema-field 'String
                       "2 letter country code"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-3166-1-alpha-2-2013)))
         :iso_3166_1_alpha_3_2013
         (schema-field 'String
                       "3 letter country code"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-3166-1-alpha-3-2013)))
         :iso_639_3_alpha_2_2007
         (deprecate-schema-field
           "Incorrect ISO name, use iso-639-1-2002 instead. It will be removed."
           (schema-field 'String
                         "2 letter language code"
                         (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-639-3-alpha-2-2007))))
         :iso_639_3_alpha_3_2007
         (deprecate-schema-field
           "Incorrect ISO name, use iso-639-2-1998 instead. It will be removed."
           (schema-field 'String
                         "3 letter language code"
                         (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-639-3-alpha-3-2007))))
         :iso_639_1_2002
         (schema-field 'String
                       "2 letter language code"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-639-1-2002)))
         :iso_639_2_1998
         (schema-field 'String
                       "3 letter language code"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-639-2-1998)))
         :iso_639_3_2007
         (schema-field 'String
                       "3 letter language code"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-639-3-2007)))
         :sun_education_field_code_2020
         (schema-field 'String
                       "SUN education field code that can be found on concepts of `sun-education-field-*` types"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/sun-education-field-code-2020)))
         :sun_education_level_code_2020
         (schema-field 'String
                       "SUN education level code that can be found on concepts of `sun-education-level-*` types"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/sun-education-level-code-2020)))
         :sun_education_field_code_2000
         (schema-field 'String
                       "Old SUN education field code 2000 that can be found on concepts of `sun-education-field-*` types"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/sun-education-field-code-2000)))
         :sun_education_level_code_2000
         (schema-field 'String
                       "Old SUN education level code that can be found on concepts of `sun-education-level-*` types"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/sun-education-level-code-2000)))
         :sni_level_code_2007
         (schema-field 'String
                       "SNI level code that can be found on concepts of `sni-level-*` types"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/sni-level-code-2007)))

         :unemployment_type_code
         (schema-field 'String
                       "Unemployment type code"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/unemployment-type-code)))

         :unemployment_fund_code
         (schema-field 'String
                       "Unemployment fund code"
                       (batch-resolver (fetch-concepts-attr :concept.external-standard/unemployment-fund-code-2017)))

         :deprecated
         (schema-field '(non-null Boolean)
                       "Indicator whether the concept is deprecated"
                       (batch-resolver (fetch-concepts-attr :concept/deprecated false)))
         :deprecated_legacy_id
         (deprecate-schema-field
           "This value is needed for Arbetsförmedlingen's internal systems during migration period. It will be removed."
           (schema-field 'String
                         "Concept ID that was imported from the old Arbetsförmedlingen's taxonomy. Uniqueness is per type only"
                         (batch-resolver fetch-legacy-ids)))
         :substitutability_percentage
         (schema-field 'Int
                       "Substitutability percentage, an int between 0 and 100. Can appear only on concepts inside `substitutes` or `substituted_by` scopes"
                       (batch-resolver (fetch-relation-attr :substitutability_percentage)))
         :replaces
         (schema-field '(non-null (list (non-null :Concept)))
                       "Concepts this concept replaces (that are most probably deprecated, so this field includes deprecated concepts by default)"
                       (batch-resolver fetch-replaces)
                       schema-concept-arguments)
         :replaced_by
         (schema-field '(non-null (list (non-null :Concept)))
                       "Concepts that replace this concept (that is most probably deprecated: don't forget to add `include_deprecated` argument when querying it)"
                       (batch-resolver fetch-replaced-by)
                       schema-concept-arguments)}
        (relation-field-definitions))}}}))

(defn execute [query variables operation-name api-key]
  (let [result (l/execute schema query variables {::api-key api-key} {:operation-name operation-name})
        stop-superlifter (::stop-superlifter (:extensions result))]
    (when stop-superlifter (stop-superlifter))
    (dissoc result :extensions)))

;; todo can we optimise performing multiple queries on a single layer by performing single query? Like loading all concept types at once...
