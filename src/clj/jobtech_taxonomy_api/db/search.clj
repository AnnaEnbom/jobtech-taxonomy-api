(ns jobtech-taxonomy-api.db.search
  (:refer-clojure :exclude [type])
  (:require [jobtech-taxonomy-api.db.api-util :as api-util]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.database-connection :as db]
            [jobtech-taxonomy-api.db.popularity :as pop]
            [jobtech-taxonomy-common.relation :as relation]
            [schema.core :as s]))

(def initial-concept-query
  '{:find [(pull ?c [:concept/id
                     :concept/type
                     :concept/preferred-label])]
    :in [$ % ?q]
    :args []
    :where [(not [?c :concept/deprecated true])
            [?c :concept/preferred-label ?preferred-label]
            [(.matches ^String ?preferred-label ?q)]]
    :offset 0
    :limit -1})

(defn remap-query
  [{args :args offset :offset limit :limit :as m}]
  {:query (-> m
              (dissoc :args)
              (dissoc :offset)
              (dissoc :limit))
   :args args
   :offset offset
   :limit limit})
;; relation related-ids


(defn- fetch-concepts [q type relation related-ids search-type version]

  (cond-> initial-concept-query

    true
    (-> (update :args conj (db/get-db version) relation/rules)
        (update :args conj (if (= "starts-with" search-type)
                             (api-util/str-to-pattern-starts-with q)
                             (api-util/str-to-pattern-contains q))))

    type
    (-> (update :in conj '[?type ...])
        (update :args conj type)
        (update :where conj '[?c :concept/type ?type]))

    (and relation related-ids)
    (concepts/handle-relations relation related-ids)

    true
    remap-query))

(def get-concepts-by-search-schema
  "The response schema for the query below."
  [{:id s/Str
    :type s/Str
    (s/optional-key :preferred-label) s/Str}])

(defn get-concepts-by-search [q type relation related-ids search-type offset limit version]
  (let [search
        (api-util/parse-seach-concept-datomic-result
         (db/q
          (fetch-concepts q type relation related-ids search-type version)))
        pop (pop/fetch-popularity)]
    (->> search
         (map #(assoc % :popularity (get pop (:concept/id %) 0)))
         (sort-by :popularity #(compare %2 %1))
         (map #(dissoc % :popularity))
         (api-util/pagination offset limit))))