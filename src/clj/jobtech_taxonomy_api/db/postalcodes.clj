(ns jobtech-taxonomy-api.db.postalcodes
  (:require
   [jobtech-taxonomy-legacy-postalcodes.core :as pc]))

(defn fetch-postalcodes []
  pc/postalcodes)
